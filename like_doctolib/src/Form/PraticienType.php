<?php

namespace App\Form;

use App\Entity\Praticien;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class PraticienType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {


        

        $builder
        ->add('Nom', TextType::class, [
            'constraints' => [
                new Assert\Regex([
                    'pattern' => '/^[a-zA-Z]{2,}$/',
                    'message' => 'Le nom ne peut contenir que des lettres et doit comporter au moins deux caractères.',
                ]),
            ]
        ])
        ->add('Prenom', TextType::class, [
            'constraints' => [
                new Assert\Regex([
                    'pattern' => '/^[a-zA-Z]{2,}$/',
                    'message' => 'Le prenom ne peut contenir que des lettres et doit comporter au moins deux caractères.',
                ]),
            ]
        ])
        ->add('Telephone', TextType::class, [
            'constraints' => [
                new Assert\Regex([
                    'pattern' => '/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/',
                    'message' => 'Le numéro de téléphone n\'est pas au bon format.',
                ]),
                new Assert\Callback([
                    'callback' => function ($value, ExecutionContextInterface $context) {
                        if ($value === '0000000000') {
                            $context->buildViolation('Vrais numero de tel obligatoire.')
                                ->addViolation();
                        }
                    }
                ])
            ]
        ])
        ->add('Adresse')
        ->add('Ville', TextType::class, [
            'constraints' => [
                new Assert\Regex([
                    'pattern' => '/^\d{5}$/',
                    'message' => 'Le code postal doit être composé de 5 chiffres.'
                ]),
                new Assert\Callback([
                    'callback' => function ($value, ExecutionContextInterface $context) {
                        if ($value === '00000') {
                            $context->buildViolation('Le code postal ne peut pas être "00000".')
                                ->addViolation();
                        }
                    }
                ])
            ]
        ])
            ->add('specialite') 
        ;   
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Praticien::class,
        ]);
    }
}
