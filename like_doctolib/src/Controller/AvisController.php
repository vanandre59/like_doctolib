<?php

namespace App\Controller;

use App\Entity\Praticien;
use App\Form\AvisType;
use App\Entity\Avis;
use App\Repository\AvisRepository;
use App\Repository\PraticienRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AvisController extends AbstractController
{
    #[Route('/avis', name: 'app_avis')]
    public function index(Request $request, PraticienRepository $praticienRepo, AvisRepository $avisRepository): Response
    {
        $avis = new Avis();
        $form = $this->createForm(AvisType::class, $avis);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $post = $_POST;
            $avis->setNote($post['avis']['Note'])
               ->setPraticien($praticienRepo)
             ->setPatient($this->getUser()->getPatient());
            $avisRepository->save($avis, true);
            return $this->redirectToRoute('app_praticien_index');}
            return $this->render('avis/index.html.twig', [
                'form' => $form->createView(),
            ]);
        
    }
}
