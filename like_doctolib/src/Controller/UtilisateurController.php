<?php

namespace App\Controller;


use Symfony\Config\SecurityConfig;
use Symfony\Component\Security\Core\Security;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class UtilisateurController extends AbstractController
    {
        /**
     * @Route("/utilisateur", name="app_utilisateur")
     */
    public function index(Security $security, EntityManagerInterface $entityManager): Response
    {
        $user = $security->getUser();
        $utilisateur = $entityManager->getRepository(User::class)->find($user->getId());
        
        $roles = ['ROLE_PATIENT', 'ROLE_PRATICIEN'];
        
        foreach ($roles as $role) {
            if (in_array($role, $utilisateur->getRoles())) {
                switch ($role) {
                    case 'ROLE_PATIENT':
                        return $this->redirectToRoute('app_patient_new');
                        break;
                    case 'ROLE_PRATICIEN':
                        return $this->redirectToRoute('app_praticien_new');
                        break;
                }
            }
        }
        
        throw new \Exception('Unable to determine role for user.');
    }   
    
    }
