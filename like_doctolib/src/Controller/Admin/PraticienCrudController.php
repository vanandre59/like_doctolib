<?php

namespace App\Controller\Admin;

use App\Entity\Praticien;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PraticienCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Praticien::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Praticiens')
            ->setEntityLabelInSingular('Praticien')
            ->setPageTitle("index", "LikeDoctolib - Administration des Praticien")
            ->setPaginatorPageSize(10);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('Nom'),
            TextField::new('Prenom'),
            TextField::new('Telephone'),
            TextField::new('Adresse'),

        ];
    }

}
