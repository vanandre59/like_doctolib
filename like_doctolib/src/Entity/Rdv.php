<?php

namespace App\Entity;

use App\Repository\RdvRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RdvRepository::class)]
class Rdv
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?bool $Creneau = null;

    #[ORM\ManyToOne(inversedBy: 'rdvs')]
    private ?Patient $Patient = null;

    #[ORM\ManyToOne(inversedBy: 'rdvs')]
    private ?Praticien $Praticien = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isCreneau(): ?bool
    {
        return $this->Creneau;
    }

    public function setCreneau(bool $Creneau): self
    {
        $this->Creneau = $Creneau;

        return $this;
    }

    public function getPatient(): ?Patient
    {
        return $this->Patient;
    }

    public function setPatient(?Patient $Patient): self
    {
        $this->Patient = $Patient;

        return $this;
    }

    public function getPraticien(): ?Praticien
    {
        return $this->Praticien;
    }

    public function setPraticien(?Praticien $Praticien): self
    {
        $this->Praticien = $Praticien;

        return $this;
    }
}
