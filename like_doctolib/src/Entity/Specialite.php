<?php

namespace App\Entity;

use App\Repository\SpecialiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SpecialiteRepository::class)]
class Specialite
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $Nom = null;

    #[ORM\OneToMany(mappedBy: 'specialite', targetEntity: Praticien::class)]
    private Collection $Praticien;

    public function __construct()
    {
        $this->Praticien = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    /**
     * @return Collection<int, Praticien>
     */
    public function getPraticien(): Collection
    {
        return $this->Praticien;
    }

    public function addPraticien(Praticien $praticien): self
    {
        if (!$this->Praticien->contains($praticien)) {
            $this->Praticien->add($praticien);
            $praticien->setSpecialite($this);
        }

        return $this;
    }

    public function removePraticien(Praticien $praticien): self
    {
        if ($this->Praticien->removeElement($praticien)) {
            // set the owning side to null (unless already changed)
            if ($praticien->getSpecialite() === $this) {
                $praticien->setSpecialite(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->Nom;
    }
    
}
