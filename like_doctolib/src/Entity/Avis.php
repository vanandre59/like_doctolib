<?php

namespace App\Entity;

use App\Repository\AvisRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AvisRepository::class)]
class Avis
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 2, scale: 1)]
    private ?string $Note = null;

    #[ORM\ManyToOne(inversedBy: 'avis')]
    private ?Patient $Patient = null;

    #[ORM\ManyToOne(inversedBy: 'avis')]
    private ?Praticien $Praticien = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNote(): ?string
    {
        return $this->Note;
    }

    public function setNote(string $Note): self
    {
        $this->Note = $Note;

        return $this;
    }

    public function getPatient(): ?Patient
    {
        return $this->Patient;
    }

    public function setPatient(?Patient $Patient): self
    {
        $this->Patient = $Patient;

        return $this;
    }

    public function getPraticien(): ?Praticien
    {
        return $this->Praticien;
    }

    public function setPraticien(?Praticien $Praticien): self
    {
        $this->Praticien = $Praticien;

        return $this;
    }
}
