<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230203151030 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE patient_praticien DROP FOREIGN KEY FK_AB11F5EC2391866B');
        $this->addSql('ALTER TABLE patient_praticien DROP FOREIGN KEY FK_AB11F5EC6B899279');
        $this->addSql('ALTER TABLE praticien_patient DROP FOREIGN KEY FK_A877B1EF2391866B');
        $this->addSql('ALTER TABLE praticien_patient DROP FOREIGN KEY FK_A877B1EF6B899279');
        $this->addSql('DROP TABLE patient_praticien');
        $this->addSql('DROP TABLE praticien_patient');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE patient_praticien (patient_id INT NOT NULL, praticien_id INT NOT NULL, INDEX IDX_AB11F5EC6B899279 (patient_id), INDEX IDX_AB11F5EC2391866B (praticien_id), PRIMARY KEY(patient_id, praticien_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE praticien_patient (praticien_id INT NOT NULL, patient_id INT NOT NULL, INDEX IDX_A877B1EF6B899279 (patient_id), INDEX IDX_A877B1EF2391866B (praticien_id), PRIMARY KEY(praticien_id, patient_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE patient_praticien ADD CONSTRAINT FK_AB11F5EC2391866B FOREIGN KEY (praticien_id) REFERENCES praticien (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE patient_praticien ADD CONSTRAINT FK_AB11F5EC6B899279 FOREIGN KEY (patient_id) REFERENCES patient (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE praticien_patient ADD CONSTRAINT FK_A877B1EF2391866B FOREIGN KEY (praticien_id) REFERENCES praticien (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE praticien_patient ADD CONSTRAINT FK_A877B1EF6B899279 FOREIGN KEY (patient_id) REFERENCES patient (id) ON DELETE CASCADE');
    }
}
