<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230203141537 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD fk_patient_id INT DEFAULT NULL, ADD fk_praticien_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6499BE758EA FOREIGN KEY (fk_patient_id) REFERENCES patient (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64924C8472 FOREIGN KEY (fk_praticien_id) REFERENCES praticien (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6499BE758EA ON user (fk_patient_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64924C8472 ON user (fk_praticien_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6499BE758EA');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64924C8472');
        $this->addSql('DROP INDEX UNIQ_8D93D6499BE758EA ON user');
        $this->addSql('DROP INDEX UNIQ_8D93D64924C8472 ON user');
        $this->addSql('ALTER TABLE user DROP fk_patient_id, DROP fk_praticien_id');
    }
}
