<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230203134004 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE favoris_patient DROP FOREIGN KEY FK_AF307EEE51E8871B');
        $this->addSql('ALTER TABLE favoris_patient DROP FOREIGN KEY FK_AF307EEE6B899279');
        $this->addSql('ALTER TABLE favoris_praticien DROP FOREIGN KEY FK_DFE77D302391866B');
        $this->addSql('ALTER TABLE favoris_praticien DROP FOREIGN KEY FK_DFE77D3051E8871B');
        $this->addSql('DROP TABLE favoris');
        $this->addSql('DROP TABLE favoris_patient');
        $this->addSql('DROP TABLE favoris_praticien');
        $this->addSql('ALTER TABLE praticien ADD note NUMERIC(2, 1) NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE favoris (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE favoris_patient (favoris_id INT NOT NULL, patient_id INT NOT NULL, INDEX IDX_AF307EEE6B899279 (patient_id), INDEX IDX_AF307EEE51E8871B (favoris_id), PRIMARY KEY(favoris_id, patient_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE favoris_praticien (favoris_id INT NOT NULL, praticien_id INT NOT NULL, INDEX IDX_DFE77D3051E8871B (favoris_id), INDEX IDX_DFE77D302391866B (praticien_id), PRIMARY KEY(favoris_id, praticien_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE favoris_patient ADD CONSTRAINT FK_AF307EEE51E8871B FOREIGN KEY (favoris_id) REFERENCES favoris (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE favoris_patient ADD CONSTRAINT FK_AF307EEE6B899279 FOREIGN KEY (patient_id) REFERENCES patient (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE favoris_praticien ADD CONSTRAINT FK_DFE77D302391866B FOREIGN KEY (praticien_id) REFERENCES praticien (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE favoris_praticien ADD CONSTRAINT FK_DFE77D3051E8871B FOREIGN KEY (favoris_id) REFERENCES favoris (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE praticien DROP note');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
    }
}
